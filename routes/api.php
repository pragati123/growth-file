<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/feedback', 'FeedbackController@store');
Route::get('/feedback', 'FeedbackController@listOfFeedbacks');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('location-distance', 'LocationController@getDistance');
Route::post('sheet-download', 'FeedbackController@downloadSheet');
Route::post('update', 'FeedbackController@handleSheetWebhook');
Route::any('image', 'FeedbackController@defaultImage');
Route::any('duration', 'FeedbackController@durationInMinutes');

Route::any('hypertrack-webhook', 'FeedbackController@location');
