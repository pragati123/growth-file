<?php

use App\Feedback;
use Illuminate\Database\Seeder;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Feedback::truncate();

        for ($i = 0; $i <= 100; $i++) {

           $feedback = new Feedback();
           $feedback->name = $faker->name();
           $feedback->email = $faker->email;
           $feedback->save();
        }

    }
}
