<?php

namespace App\Http\Requests;


class LocationDistanceRequest extends Request
{

    const LAT1 = "lat1";
    const LAT2 = "lat2";
    const LON1 = "lon1";
    const LON2 = "lon2";
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::LAT1 => 'required|latitude',
            self::LAT2 => 'required|latitude',
            self::LON1 => 'required|longitude',
            self::LON2 => 'required|longitude',
        ];
    }

    public function getLat1(){
        return $this->get(self::LAT1);
    }

    public function getLat2(){
        return $this->get(self::LAT2);
    }

    public function getLon1(){
        return $this->get(self::LON1);
    }

    public function getLon2(){
        return $this->get(self::LON2);
    }
}
