<?php
/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 1/9/17
 * Time: 4:44 PM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{
    public function wantsJson()
    {
        return true;
    }

    public function expectsJson()
    {
        return true;
    }
}