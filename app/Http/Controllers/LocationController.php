<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationDistanceRequest;
use Curl\Curl;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class LocationController extends Controller
{
    const LATITUDE_REGEX_RULE = '/^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/';
    const LONGITUDE_REGEX_RULE = '/^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/';

    public function getDistance(Request $request)
    {
        $this->validate($request, [
            "lat1" => [
                'required',
                'regex:' . self::LATITUDE_REGEX_RULE
            ],
            "lat2" => [
                'required',
                'regex:' . self::LATITUDE_REGEX_RULE
            ],
            "lon1" => [
                'required',
                'regex:' . self::LONGITUDE_REGEX_RULE
            ],
            "lon2" => [
                'required',
                'regex:' . self::LONGITUDE_REGEX_RULE
            ],
        ]);

        $lat1 = $request->get("lat1");
        $lat2 = $request->get("lat2");
        $lon1 = $request->get("lon1");
        $lon2 = $request->get("lon2");

        $aerialDistance = $this->calculateDistance($lat1, $lon1, $lat2, $lon2) * 1000;

        if ($aerialDistance < 500) {
            return response()->json([
                'message' => 'The aerial distance was less than 500 meters.',
                'distance' => 0
            ]);
        } else {
            $curl = new Curl();
            $curl->get("https://maps.googleapis.com/maps/api/distancematrix/json", [
                "origins" => "$lat1,$lon1",
                "destinations" => "$lat2,$lon2"
            ]);
            $response = json_decode($curl->rawResponse, true);
            $element = $response['rows'][0]['elements'][0];
            if ($response["status"] == 'OK') {
                if ($element['status'] == 'ZERO_RESULTS') {
                    throw new UnprocessableEntityHttpException('No Route found between the locations');
                } else if ($element['status'] == 'NOT_FOUND') {
                    throw new UnprocessableEntityHttpException('Origin and destination could not be geocoded');
                } else if ($element['status'] == 'MAX_ROUTE_LENGTH_EXCEEDED') {
                    throw new UnprocessableEntityHttpException('Request is too long to process');
                } else {
                    $value = $element["distance"]["value"];
                    return response()->json([
                        'message' => 'The road distance is calculated using Google Map API.',
                        'distance' => $value
                    ]);
                }
            } else {
                return response()->json($curl->rawResponse);
            }


        }
    }

    public function calculateDistance($lat1, $lon1, $lat2, $lon2)
    {
        if ($lat1 == $lat2 && $lon1 == $lon2) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            $distance = (round(($miles * 1.609344), 2));
            if ($distance == 0) {
                return 0;
            } else {
                return "$distance";
            }
        }
    }

}
