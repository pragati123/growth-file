<?php

namespace App\Http\Controllers;

use App\CircleCrop;
use App\Feedback;
use Carbon\Carbon;
use Curl\Curl;
use DateTime;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_SpreadsheetProperties;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Imagick;
use ImagickDraw;
use ImagickPixel;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        $feedback = new Feedback();

        $feedback->name = $request->get("name");
        $feedback->email = $request->get("email");

        $feedback->save();

        return response()->json($feedback);

    }

    public function listOfFeedbacks()
    {

        $feedbacks = Feedback::all();
        return response()->json($feedbacks);

    }

    public function downloadSheet()
    {
        $client = new Google_Client();
        $client->setApplicationName('Simplifii');
        $client->setScopes(array(
                ' https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/spreadsheets')
        );
        $client->setAuthConfig(base_path('client_secret.json'));
        $client->setAccessType('offline');

        $service = new Google_Service_Sheets($client);

        $requestBody = new Google_Service_Sheets_Spreadsheet();
        $properties = new Google_Service_Sheets_SpreadsheetProperties;
        $properties->setTitle('Simplifii');
        $requestBody->setProperties($properties);


        $response = $service->spreadsheets->create($requestBody);
        $id = $response->getSpreadsheetId();
        $a = new \Google_Service_Drive($client);
        $b = new \Google_Service_Drive_Permission();
        $b->setType('anyone');
        $b->setRole('writer');
        $a->permissions->create($id, $b);


        return $response->getSpreadsheetUrl();

    }

    public function handleSheetWebhook(Request $request)
    {
        try {
            $row = $request->get("row");
            $column = $request->get("column");
            $value = $request->get("value");
            $feedback = Feedback::find($row - 1);

            if ($column == 1) {
                $feedback->name = $value;
            } else {
                $feedback->email = $value;
            }

            $feedback->save();
            Log::critical("Succesfully handeled api");

        } catch (\Exception $e) {
            Log::critical('Error => ' . $e->getMessage());
        }
    }

    public function defaultImage(Request $request)
    {
        $name = $request->get('name');
        $name_arrray = explode(' ', $name);

        if (count($name_arrray) > 1) {
            $default_pic_text = $name_arrray[0][0] . $name_arrray[1][0];
        } else {
            $default_pic_text = $name_arrray[0][0];
        }

        $color = $this->random_color();

        $url = "http://chat.connectinn.tk/128x128/" . $color . "/ffffff.png&text=" . strtoupper($default_pic_text);
//        dd($url);

        $image = imagecreatefrompng($url);

        $width = imagesx($image);
        $height = imagesy($image);
        $crop = new CircleCrop($image, $width, $height);
        return $crop->crop()->display();

    }

    function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public static function location($request)
    {

        $decoded_json = $request->all();

        $data = [];
        $data['entity'] = "Location";
        $data['action'] = "Create";
        if (isset($decoded_json->user_id)) {
            $data['ht_userid'] = $decoded_json->user_id;
        }
        if (isset($decoded_json->data->object->location_status)) {
            $location_status = $decoded_json->data->object->location_status;
            if ($location_status == "location_available") {
                $data['gps_status'] = true;
            } else {
                $data['gps_status'] = false;
            }

        }
        if (isset($decoded_json->type)) {
            $type = $decoded_json->type;
            $data['label'] = $type;

            if ($type = "user.created") {
                $name = $decoded_json->data->object->name;
                $ht_user_id = $decoded_json->user_id;
                $card = DB::table('cards')->where('string1', '=', $name)->where('entity', '=', 'Employee')->where('fk_org', '=', '22')->first();
                DB::table('htusers')->insert([
                    'fk_user' => $card->id,
                    'ht_user' => $ht_user_id
                ]);
                $data['whose_location'] = $card->id;
            } else {
                $htUser = DB::table('htusers')->where('ht_user', $decoded_json->user_id)->first();
                $data['whose_location'] = $htUser->fk_user;
            }


            if ($type == "trip.started" || $type == "trip.ended" || $type == "stop.started" || $type == "stop.ended") {
                if (isset($decoded_json->data->object->location->recorded_at)) {
                    $data['recorded_at'] = Carbon::parse($decoded_json->data->object->location->recorded_at)->toDateString();
                }

                if (isset($decoded_json->data->object->location->provider)) {
                    $provider = $decoded_json->data->object->location->provider;
                    if ($provider == "non_gps") {
                        $data['location_picked_from_gps'] = false;
                    } else {
                        $data['location_picked_from_gps'] = true;
                    }
                }

                if (isset($decoded_json->data->object->location->geojson->coordinates[1])) {
                    $data['lat'] = $decoded_json->data->object->location->geojson->coordinates[1];
                }

                if (isset($decoded_json->data->object->location->geojson->coordinates[0])) {
                    $data['lng'] = $decoded_json->data->object->location->geojson->coordinates[0];
                }


            } else {
                if (isset($decoded_json->data->object->last_location->geojson->coordinates[1])) {
                    $data['lat'] = $decoded_json->data->object->last_location->geojson->coordinates[1];
                }

                if (isset($decoded_json->data->object->last_location->geojson->coordinates[0])) {
                    $data['lng'] = $decoded_json->data->object->last_location->geojson->coordinates[0];
                }

                if (isset($decoded_json->data->object->last_location->provider)) {
                    $provider = $decoded_json->data->object->last_location->provider;
                    if ($provider == "non_gps") {
                        $data['location_picked_from_gps'] = false;
                    } else {
                        $data['location_picked_from_gps'] = true;
                    }
                }

                if (isset($decoded_json->data->object->last_location->recorded_at)) {
                    $temp = $decoded_json->data->object->last_location->recorded_at;
                    $data['recorded_at'] = Carbon::parse($temp)->toDateString();
                }

            }
        }
        dd($data);

        $json_data = json_encode($data);

        ////Cache::forever('botToken', "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwMDU2LCJpc3MiOiJodHRwOi8vcm1zLmRlbW9wbGF0Zm9ybS5zaW1wbGlmaWkuY29tL2FwaS92MS9hZG1pbi9hdXRoZW50aWNhdGUiLCJpYXQiOjE1MDkwMjg0ODIsImV4cCI6MTUwOTYzMzI4MiwibmJmIjoxNTA5MDI4NDgyLCJqdGkiOiJuRUFEUnQ3QkVaQ3BSeUNJIn0.OM2IPqwPjm3z-VjePtqyH3E4U1RmtJQf9gUxtdgQ4I8")

        $token = "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwMDU2LCJpc3MiOiJodHRwOi8vcm1zLmRlbW9wbGF0Zm9ybS5zaW1wbGlmaWkuY29tL2FwaS92MS9hZG1pbi9hdXRoZW50aWNhdGUiLCJpYXQiOjE1MDkwMjg0ODIsImV4cCI6MTUwOTYzMzI4MiwibmJmIjoxNTA5MDI4NDgyLCJqdGkiOiJuRUFEUnQ3QkVaQ3BSeUNJIn0.OM2IPqwPjm3z-VjePtqyH3E4U1RmtJQf9gUxtdgQ4I8";


        try {
            $client = new Client([
                'headers' => ['Authorization' => $token, "Content-Type" => "application/json"]
            ]);
            $response = $client->post("http://rms.demoplatform.simplifii.com/api/v1/cards", ['body' => $json_data]);

        } catch (BadResponseException $e) {
            $res = json_decode($e->getResponse()->getBody()->getContents(), true);
            Log::critical('Api not called properly => ' . $e->getMessage());
        }

        /* $curl = new Curl();
         $curl->setHeaders([
             "Authorization" => "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwMDU2LCJpc3MiOiJodHRwOi8vcm1zLmRlbW9wbGF0Zm9ybS5zaW1wbGlmaWkuY29tL2FwaS92MS9hZG1pbi9hdXRoZW50aWNhdGUiLCJpYXQiOjE1MDkwMjg0ODIsImV4cCI6MTUwOTYzMzI4MiwibmJmIjoxNTA5MDI4NDgyLCJqdGkiOiJuRUFEUnQ3QkVaQ3BSeUNJIn0.OM2IPqwPjm3z-VjePtqyH3E4U1RmtJQf9gUxtdgQ4I8",
             "Content-Type" => "application/json"
         ]);

         $curl->post("http://rms.demoplatform.simplifii.com/api/v1/cards", $data);
         $curl->close();*/
    }

    private function markLocation($data)
    {
        $curl = new Curl();
        $curl->setHeaders([
            "Authorization" => Cache::get('botToken'),
            "Content-Type" => "application/json"
        ]);

        $curl->post("http://rms.demoplatform.simplifii.com/api/v1/cards", $data);
        if ($curl->error) {
            if ($curl->httpStatusCode == 401) {
                try {
                    $this->loginBot();
                    $this->markLocation($data);
                } catch (\Exception $e) {
                    $curl->close();
                    Log::critical('Could not mark Location, message => ' . $e->getMessage());
                }
            }
        }
        Log::critical($curl->response);
        // dd($curl->response);
        $curl->close();
    }

    private function loginBot()
    {
        $bot_email = "rmsbot@example.com";
        $bot_password = "111111";
        $curl = new Curl();
        $curl->post("http://rms.demoplatform.simplifii.com/api/v1/admin/authenticate", [
            'email' => $bot_email,
            'password' => $bot_password
        ]);

        if (!$curl->error) {
            $response = $curl->response;
            Cache::forever('botToken', 'bearer ' . $response->token);
        } else {
            $curl->close();
            throw new \Exception('Could Not Login Bot, with message ' . $curl->errorMessage);
        }
        $curl->close();
    }

    public function printClaim(Request $request)
    {
        $this->validate($request, [
            'emp_code' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date'
        ]);
        $empCode = $request->get('emp_code');

        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $user = DB::table('cards')
            ->where('unique_code', $empCode)->first();
//        dd($user);
        $d1 = Carbon::parse($startDate);
        $d2 = Carbon::parse($endDate);

        $claims = DB::table('cards')
            ->where('fk_creator', $user->id)
            ->where('entity', 'claim')
            ->where('datetime1', '>=', $startDate)
            ->where('datetime1', '<=', $endDate)
//            ->where(function ($query) use ($d1, $d2) {
//                $query->whereDate('string1', '>=', $d1->day);
//                $query->whereDate('string1', '<=', $d2->day);
//
//            })
//            ->where(function ($query) use ($d1, $d2) {
//                $query->whereMonth('string1', '>=', $d1->month);
//                $query->whereMonth('string1', '<=', $d2->month);
//
//            })
//            ->where(function ($query) use ($d1, $d2) {
//                $query->whereYear('string1', '>=', $d1->year);
//                $query->whereYear('string1', '<=', $d2->year);
//            })
            ->orderBy('datetime1', 'asc')->get();
//    dd($request->all());
//    dd($claims);

        $nonClaimedDates = [];

        $start = \Carbon\Carbon::parse($startDate);
        $end = \Carbon\Carbon::parse($endDate);
        for ($i = $start->day, $j = 0; $i <= $end->day; $i++, $j++) {
            $flag = false;
            foreach ($claims as $claim) {
                $created = \Carbon\Carbon::parse($claim->datetime1);
                if ($created->day == $i || $claim->bool1) {
                    $flag = true;
                    break;
                }
            }
            if (!$flag) {
                array_push($nonClaimedDates, $start->copy()->addDays($j));
            }
        }

//        dd($nonClaimedDates);

        return view('print', [
            'claims' => $claims,
            'user' => $user,
            'from' => $startDate,
            'to' => $endDate,
            'nonClaimedDates' => $nonClaimedDates
        ]);
    }

}
