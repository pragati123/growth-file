<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (App::environment('local')) {
            $log = [
                'URL'=> $request->getUri(),
                'METHOD' => $request->method(),
                'REQUEST' => $request->all(),
                'RESPONSE' => $response->getContent()
            ];
            Log::info($log);
        }
        return $response;
    }
}
