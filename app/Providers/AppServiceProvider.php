<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('latitude', function ($attribute, $value) {
            return preg_match('/^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/', $value);
        });

        Validator::extend('longitude', function ($attribute, $value) {
            return preg_match('/^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
