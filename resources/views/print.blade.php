<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        @media print {
            #clocktoprint {
                display: none;
            }
        }

        #day_detail td {
            font-size: 11px;
            padding: 1px;
        }
    </style>
</head>
<body>

<div id="clocktoprint">
    <button type="button" onclick="window.print()">Click To Print!!!!</button>
</div>
<!--<div class="row" style="margin-top:10px;">
       <div class="col-xs-12" style="">&nbsp;</div>
</div>
            <div class="row" style="">
                <div class="col-xs-3" style=""><b>Claimed Amount:</b> 9555</div>
                <div class="col-xs-3" style=""><b>Employee Name:</b> Dharamvir Singh</div>
                <div class="col-xs-3" style=""><b>Employee ID:</b>..................................</div>
                <div class="col-xs-3" style=""><b>Vehicle Number:</b>..................................</div>
            </div>
-->

<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
    <div id="main-container">
        <!-- Page content -->
        <div id="page-content">
            <!-- Responsive Full Block -->
            <div class="block">
                <div class="table-responsive custom-table">

                    <!-- Header code start-->
                    <div class="block" style="border: 2px solid black;">
                        <div style="display:inline-block;width:79%;overflow:hidden;padding:0px">
                            <div class="row" style="">
                                <div class="col-xs-12" style="margin-right: 0px;">
                                    <mark style="color:white;background-color:black;">FORTNIGHTLY TOUR EXPENSE REPORT
                                    </mark>
                                    for: <b>{{$user->string1}} ({{$user->string6}}
                                        ) {{$user->bool1 == null ? ($user->bool1 ? 'Company Vehicle' : 'Personal Vehicle') : ''}}</b>
                                </div>
                            </div>
                            <div class="row" style="">
                                <div class="col-xs-3" style=""><b>Date From: {{$from}}</b></div>
                                <div class="col-xs-3" style=""><b>To: {{$to}}</b></div>
                                <div class="col-xs-3" style=""><b>State: {{$user->string4}}</b></div>
                                <div class="col-xs-3" style=""><b>H.Q.: {{$user->string5}} </b></div>
                            </div>
                        </div>
                        <div style="display:inline-block;width:10%;overflow:hidden;font-size: 9px">
                            <label><u>Underlined: Manual</u></label>
                        </div>
                        <div style="display:inline-block;width:10%;overflow:hidden;">
                            <img id="header_logo" src="http://iil.simplifii.com/configs/logo.jpg" height="46px" width="100%">
                        </div>
                    </div>
                    <!-- Header code end-->

                    <table id="general-table" class="table table-bordered table-striped table-vcenter">
                        <thead style="border: 2px solid black; font-size: 11px;">
                        <tr>
                            <th style="padding-left:25px; padding-right:25px;"> Date</th>
                            {{--<th>From</th>--}}
                            <th>Visited Location</th>
                            <th colspan="3" class="custom-th">Travel</th>
                            <th><i class="fa fa-bed"></i></th>
                            <th><i class="fa fa-envelope"></i></th>
                            <th>DA</th>
                            <th><i class="fa fa-phone"></i></th>
                            <th>M</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody id="day_detail" style="border: 2px solid black; font-size:10pt;">
                        <!-- Additional row start -->
                        <tr style="border: 1px solid black;">
                            <td></td>
                            <td class="1" style="position:relative;"></td>
                            {{--<td name="null" class="1" style="position:relative;"></td>--}}
                            <td>
                                <table class="table-bordered table-striped table-vcenter custom-table2"
                                       style="font-size:10pt;">
                                    <tbody>
                                    <tr>
                                        <td><strong>Fr</strong></td>
                                        <td><strong>To</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table-bordered table-striped table-vcenter custom-table2"
                                       style="font-size:10pt;">
                                    <tbody>
                                    <tr>
                                        <td><strong><i class="fa fa-train aFz"></i></strong></td>
                                        <td><strong><i class="fa fa-bus aFz"></i></strong></td>
                                        <td><strong><i class="fa fa-taxi"></i></strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td><b>Fuel</b></td>
                            <!--<td><b>ST</b></td>-->
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <!-- Additional row end -->
                        @php
                            $claimedTotal = 0;
                            $fuelTotal = 0;
                            $taxiTotal = 0;
                            $miscTotal = 0;
                            $travelTotal = 0;
                            $trainTotal = 0;
                            $hotelTotal = 0;
                            $DaTotal = 0;
                            $stationaryTotal = 0;
                            $phoneTotal = 0;
                        @endphp

                        @foreach($claims as $claim)
                            @continue($claim->bool1)
                            @php
                                if (!is_null($claim->cdata)) {
                                $json = json_decode($claim->cdata);
                                isset($json->misc) ? $misc = $json->misc : $misc = '';
                                isset($json->travel_to) ? $travelTo = $json->travel_to : $travelTo = '';
                                isset($json->travel_from) ? $travelFrom = $json->travel_from : $travelFrom = '';
                                isset($json->taxi) ? $taxi = $json->taxi : $taxi = '';
                                }

                            @endphp
                            <tr style="border: 1px solid black;">
                                <td>{{$claim->string1}}</td>
                                <td class="1" style="position:relative;">{{$claim->text1}}</td>
                                {{--<td name="null" class="1" style="position:relative;"></td>--}}
                                <td>
                                    <table class="table-bordered table-striped table-vcenter custom-table2"
                                           style="font-size:10pt;">
                                        <tbody>
                                        <tr>
                                            <td><strong>{{$travelFrom}}</strong></td>
                                            <td><strong>{{$travelTo}}</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class="table-bordered table-striped table-vcenter custom-table2"
                                           style="font-size:10pt;">
                                        <tbody>
                                        <tr>
                                            <td>{{$claim->int1}}</td>
                                            <td></td>
                                            <td>{{$taxi}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td><b>{{$claim->float4}}</b></td> {{-- Fuel --}}
                            <!--<td><b>ST</b></td>-->

                                <td>{{$claim->int2}}</td> {{-- Hotel --}}
                                <td>{{$claim->float1}}</td>
                                <td>{{$claim->int3}}</td> {{-- DA --}}
                                <td>{{$claim->int4}}</td>
                                <td>{{$misc}}</td>
                                @php
                                    $total = (int)$misc + $claim->int1 + (int)$taxi + $claim->float4 + $claim->int2 + $claim->float1 + $claim->int3 + $claim->int4;
                                    $claimedTotal += (int)$total;
                                    $fuelTotal += $claim->float4;
                                    $taxiTotal += (int)$taxi;
                                    $miscTotal += (int)$misc;
                                    $travelTotal += (int)((int)$travelTo - (int)$travelFrom);
                                    $trainTotal += (int)$claim->int1;
                                    $hotelTotal += $claim->int2;
                                    $DaTotal += $claim->int3;
                                    $phoneTotal += $claim->int4;
                                    $stationaryTotal += $claim->float1;
                                @endphp
                                <td>{{$total}}</td>
                            </tr>
                        @endforeach

                        {{-- Total Rows --}}

                        <tr style="border: 1px solid black;">
                            <td><b>Total</b></td>
                            <td class="1" style="position:relative;"></td>
                            {{--<td name="null" class="1" style="position:relative;"></td>--}}
                            <td>
                                <table class="table-bordered table-striped table-vcenter custom-table2"
                                       style="font-size:10pt;">
                                    <tbody>
                                    <tr>
                                        <td><strong>{{$travelTotal}}</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table-bordered table-striped table-vcenter custom-table2"
                                       style="font-size:10pt;">
                                    <tbody>
                                    <tr>
                                        <td><strong>{{$trainTotal}}</strong></td>
                                        <td><strong><i class="fa fa-bus aFz"></i></strong></td>
                                        <td><strong>{{$taxiTotal}}</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td><b>{{$fuelTotal}}</b></td>
                            <!--<td><b>ST</b></td>-->
                            <td><b>{{$hotelTotal}}</b></td>
                            <td><b>{{$stationaryTotal}}</b></td>
                            <td><b>{{$DaTotal}}</b></td>
                            <td><b>{{$phoneTotal}}</b></td>
                            <td><b>{{$miscTotal}}</b></td>
                            <td><b>{{$claimedTotal}}</b></td>
                        </tr>


                        <tr style="border: 1px solid black;">
                            <td colspan="11">
                                No Claims submitted by Employee on -
                                @foreach($nonClaimedDates as $date)
                                    {{$date->day}} {{$date->format('F')}},
                                @endforeach
                            </td>
                        </tr>


                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="" id="calculated_values">
    <div class="col-xs-4" style=""><b>Claimed Amount: {{$claimedTotal}}</b></div>
    <div class="col-xs-4" style=""><b>Kms. Done: {{$travelTotal}}</b></div>
</div>
<div class="row" style="" id="calculated_values">
    <div class="col-xs-4" style=""><b>Employee's Signature:...............</b></div>
    <div class="col-xs-4" style=""><b>Fuel Used: {{$fuelTotal}}</b></div>
    <div class="col-xs-4" style=""><b>Approver’s Name & Date: Rupender Verma - 02-11-2017</b></div>
</div>
<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.8.3.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/app.js"></script>
<script type="text/javascript" src="configs/config_file.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/simpli-global.js"></script>

<script type="text/javascript">
    //retreiving AUTHTOKEN
    var cookie_name = "AUTH_TOKEN_EMP";
    //console.log($.cookie("AUTH_TOKEN_EMP"));
    var res = $.cookie(cookie_name);
    if (typeof res == 'undefined' || res.length <= 0) {
        //                        window.location = "index.php";
    }


    $(document).ready(function () {
        if (window.location.href.indexOf("?") > 0) {
            //  var subid = getUrlParameters("sub_id", "", true);
            // subid = Base64.decode(subid);
            // var uid = getUrlParameters("u_id", "", true);
            // uid = Base64.decode(uid);
            var subid = "73";
            var uid = "153";
        }
        var output;
        var location_from;
        var location_to;
        var bike_car_amount_claimed;
        var total_claim_amount;
        var train_amount_claimed;
        var bus_amount_claimed;
        var fuel_amount_claimed;
        var travel_amount_claimed;
        var hotel_amount_claimed;
        var air_amount_claimed;
        var daily_allowance_amount_claimed;
        var telephone_postage_amount_claimed;
        var misc_amount_claimed;
        var auto_taxi_claimed;
        var toll_amount_claimed;
        var fuel_claimed_amount1;
        var travel_amount_claimed1;
        var meter_from1;
        var meter_to1;
        //var uid = 507;
        //var subid = 29;
        // Additional variables
        var total_distance = 0;
        var total_tr = 0;
        var total_bus = 0;
        var total_fl = 0;
        var total_au = 0;
        var total_cv = 0;
        var total_pv = 0;
        var total_st = 0;
        var total_hotel = 0;
        var total_toll = 0;
        var total_da = 0;
        var total_tel = 0;
        var total_misc = 0;
        var net_total = 0;
        var vehicle_type;
        var personal_total;
        var approval_details;
        var sub_status;
        $.ajax({
            type: 'POST',
            url: ajaxUrl + '/approver/submissiondaydetail',
            async: true,
            crossDomain: true,
            data: "user_id=" + uid + "&submission_cycle_id=" + subid,
            dataType: 'json',
            headers: {"API_KEY": apiKey, "AUTH_TOKEN": res},
            success: function (responsedata, status, xhr) {
                //console.log(responsedata);
                result = JSON.stringify(responsedata);
                //console.log(result);
                var i = 1;
                var fnc = " ";
                var zeroc = " ";
                var cur_authtoken = $.cookie("AUTH_TOKEN_EMP");
                config_status(responsedata, cur_authtoken);
                var respnsestatus = xhr.getResponseHeader('RESPONSE_CODE');
                if (respnsestatus == 1) {
                    approval_details = responsedata.data.response.approval_details;
                    sub_status = responsedata.data.response.submission_circle.submission_status_id;

                    if (responsedata.data.response.submission_circle.submission_status_id == '2') {
                        $("#do_reject,#do_approve,#skip").show();
                        $("#back").hide();
                    }
                    $("#user_name").html(responsedata.data.response.user_detail.name);
                    $("#Submission_cycle").html(responsedata.data.response.submission_circle.start_date + '&nbsp;&nbsp;-&nbsp;&nbsp;' + responsedata.data.response.submission_circle.end_date);
                    if (responsedata.data.response.submission_day_result != "") {
                        $(responsedata.data.response.submission_day_result).each(function (index, value) {
                            travel_amount_claimed1 = 0;
                            if ((value.total_claim_amount == null || value.total_claim_amount == 0) && ((value.meter_from == null) || (value.meter_from == ""))) {
                                fncday = value.submission_day;
                                zeroc = zeroc.concat(value.submission_day);
                                zeroc = zeroc.concat(", ");
                            } else if (value.is_submission_submit == 1) {

                                if ((value.km_travel_type).toLowerCase() == "mc") {
                                    $(".vehicle").text('KMs');
                                } else if ((value.km_travel_type).toLowerCase() == "car") {
                                    $(".vehicle").text('KMs');
                                } else {

                                    $(".vehicle").text('KMs');
                                }
                                if (value.location_from == null) {
                                    location_from = "-";
                                } else {
                                    var dataIds = value.location_from_id;
                                    var ldata = value.location_from;
                                    if (dataIds != null && dataIds != "") {
                                        ldata = '<i>' + ldata + '</i>';
                                    } else {
                                        ldata = '<u>' + ldata + '</u>';
                                    }
                                    location_from = ldata;
                                }
                                if (value.location_to == null) {
                                    location_to = "-";
                                } else {
                                    var data;
                                    if (value.location_to_details.visited != '') {
                                        data = '<i>' + value.location_to_details.visited + '</i>';
                                        if (value.location_to_details.manual != '') {
                                            data += ',<u>' + value.location_to_details.manual + '</u>';
                                        }
                                    } else {
                                        data = '<u>' + value.location_to_details.manual + '</u>';
                                    }
                                    location_to = data;
                                }

                                if ((value.meter_from == null) || (value.meter_from == "")) {
                                    value.meter_from = "-";
                                    meter_from1 = 0;
                                } else {
                                    meter_from1 = value.meter_from;
                                }
                                if ((value.meter_to == null) || (value.meter_to == "")) {
                                    value.meter_to = "-";
                                    meter_to1 = 0;
                                } else {
                                    meter_to1 = value.meter_to;
                                    total_distance += (parseInt(value.meter_to) - parseInt(value.meter_from));
                                }
                                if ((value.bike_car_amount_claimed == null) || (value.bike_car_amount_claimed == "")) {
                                    bike_car_amount_claimed = "-";

                                } else {
                                    if (value.bike_car_amount_approved && value.bike_car_amount_approved != null && value.bike_car_amount_approved != value.bike_car_amount_claimed) {
                                        bike_car_amount_claimed = "<del>" + value.bike_car_amount_claimed + "</del>" + " " + value.bike_car_amount_approved;
                                    } else {
                                        bike_car_amount_claimed = value.bike_car_amount_claimed;
                                    }

                                }
                                if ((value.self_total_fare == null) || (value.self_total_fare == "")) {
                                    personal_total = '0';

                                } else {
                                    personal_total = value.self_total_fare;
                                }

                                if ((value.auto_taxi_claimed == null) || (value.auto_taxi_claimed == "")) {
                                    auto_taxi_claimed = "-";

                                } else {
                                    if (value.auto_taxi_approved && value.auto_taxi_approved != null && value.auto_taxi_approved != value.auto_taxi_claimed) {
                                        auto_taxi_claimed = "<del>" + value.auto_taxi_claimed + "</del>" + " " + value.auto_taxi_approved;
                                        total_au += parseInt(value.auto_taxi_approved);
                                    } else {
                                        auto_taxi_claimed = value.auto_taxi_claimed;
                                        total_au += parseInt(value.auto_taxi_claimed);
                                    }

                                }

                                if ((value.train_amount_claimed == null) || (value.train_amount_claimed == "")) {
                                    train_amount_claimed = "-";

                                } else {
                                    if (value.train_amount_approved && value.train_amount_approved != null && value.train_amount_approved != value.train_amount_claimed) {
                                        train_amount_claimed = "<del>" + value.train_amount_claimed + "</del>" + " " + value.train_amount_approved;
                                        total_tr += parseInt(value.train_amount_claimed);
                                    } else {
                                        train_amount_claimed = value.train_amount_claimed;
                                        total_tr += parseInt(value.train_amount_claimed);

                                    }
                                }

                                if ((value.toll_amount_claimed == null) || (value.toll_amount_claimed == "")) {
                                    toll_amount_claimed = "-";

                                } else {
                                    if (value.toll_amount_approved && value.toll_amount_approved != null && value.toll_amount_approved != value.toll_amount_claimed) {
                                        toll_amount_claimed = "<del>" + value.toll_amount_claimed + "</del>" + " " + value.toll_amount_approved;
                                        total_toll += parseInt(value.train_amount_claimed);
                                    } else {
                                        toll_amount_claimed = value.toll_amount_claimed;
                                        total_toll += parseInt(value.toll_amount_claimed);

                                    }
                                }

                                if ((value.bus_amount_claimed == null) || (value.bus_amount_claimed == "")) {
                                    bus_amount_claimed = "-";

                                } else {
                                    if (value.bus_amount_approved && value.bus_amount_approved != null && value.bus_amount_approved != value.bus_amount_claimed) {
                                        bus_amount_claimed = "<del>" + value.bus_amount_claimed + "</del>" + " " + value.bus_amount_approved;
                                        total_bus += parseInt(value.bus_amount_approved);
                                    } else {
                                        bus_amount_claimed = value.bus_amount_claimed;
                                        total_bus += parseInt(value.bus_amount_claimed);
                                    }

                                }

                                if ((value.fuel_amount_claimed == null) || (value.fuel_amount_claimed == "")) {
                                    fuel_amount_claimed = 0;
                                    fuel_claimed_amount1 = 0;

                                } else {
                                    fuel_amount_claimed = value.fuel_amount_claimed;
                                    fuel_claimed_amount1 = value.fuel_amount_claimed;
                                    total_cv += parseInt(value.fuel_amount_claimed);
                                    total_st += parseInt(value.fuel_amount_claimed);
                                }

                                if ((value.travel_amount_claimed == null) || (value.travel_amount_claimed == "")) {
                                    //travel_amount_claimed = "----";
                                    travel_amount_claimed = (parseInt(fuel_claimed_amount1) + parseInt(0));
                                    travel_amount_claimed1 = '0';


                                } else {
                                    //travel_amount_claimed = value.travel_amount_claimed;
                                    /*travel_amount_claimed = (parseInt(fuel_claimed_amount1) + parseInt(value.travel_amount_claimed));*/
                                    //travel_amount_claimed1 = parseInt(value.travel_amount_claimed);
                                    travel_amount_claimed = (parseInt(value.bus_amount_approved) + parseInt(value.auto_taxi_approved) + parseInt(value.train_amount_approved) + parseInt(value.air_amount_approved) + parseInt(value.self_total_fare));
                                    total_st += parseInt(travel_amount_claimed);
                                    //console.log(travel_amount_claimed, total_st);
                                }

                                if ((value.hotel_amount_claimed == null) || (value.hotel_amount_claimed == "")) {
                                    hotel_amount_claimed = "-";

                                } else {
                                    if (value.hotel_amount_approved && value.hotel_amount_approved != null && value.hotel_amount_approved != value.hotel_amount_claimed) {
                                        hotel_amount_claimed = "<del>" + value.hotel_amount_claimed + "</del>" + " " + value.hotel_amount_approved;
                                        total_hotel += parseInt(value.hotel_amount_approved);
                                    } else {
                                        hotel_amount_claimed = value.hotel_amount_claimed;
                                        total_hotel += parseInt(value.hotel_amount_claimed);

                                    }

                                }

                                if ((value.air_amount_claimed == null) || (value.air_amount_claimed == "")) {
                                    air_amount_claimed = "-";

                                } else {
                                    if (value.air_amount_approved && value.air_amount_approved != null && value.air_amount_approved != value.air_amount_claimed) {
                                        air_amount_claimed = "<del>" + value.air_amount_claimed + "</del>" + " " + value.air_amount_approved;
                                        total_fl += parseInt(value.air_amount_claimed);
                                    } else {
                                        air_amount_claimed = value.air_amount_claimed;
                                        total_fl += parseInt(value.air_amount_claimed);
                                    }

                                }

                                if ((value.total_claim_amount == null) || (value.total_claim_amount == "")) {
                                    total_claim_amount = "-";

                                } else {
                                    if (value.total_approved_amount && value.total_approved_amount != null && value.total_approved_amount != value.total_claim_amount) {
                                        total_claim_amount = "<del>" + value.total_claim_amount + "</del>" + " " + value.total_approved_amount;
                                        net_total += parseInt(value.total_approved_amount);
                                    } else {
                                        total_claim_amount = value.total_claim_amount;
                                        net_total += parseInt(value.total_claim_amount);
                                    }

                                }

                                if ((value.daily_allowance_amount_claimed == null) || (value.daily_allowance_amount_claimed == "") || (value.daily_allowance_amount_claimed == 0)) {
                                    //  daily_allowance_amount_claimed = "----";
                                    if ((value.daily_allowance_loc_amount_claimed == null) || (value.daily_allowance_loc_amount_claimed == "")) {
                                        daily_allowance_amount_claimed = 0;
                                    } else {
                                        if (value.daily_allowance_loc_amount_approved && value.daily_allowance_loc_amount_approved != null && value.daily_allowance_loc_amount_approved != value.daily_allowance_loc_amount_claimed) {
                                            daily_allowance_amount_claimed = "<del>" + value.daily_allowance_loc_amount_claimed + "</del>" + " " + value.daily_allowance_loc_amount_approved;
                                            total_da += parseInt(value.daily_allowance_loc_amount_approved);
                                        } else {
                                            daily_allowance_amount_claimed = value.daily_allowance_loc_amount_claimed;
                                            total_da += parseInt(value.daily_allowance_loc_amount_claimed);
                                        }


                                    }

                                } else {
                                    if (value.daily_allowance_amount_approved && value.daily_allowance_amount_approved != null && value.daily_allowance_amount_approved != value.daily_allowance_amount_claimed) {
                                        daily_allowance_amount_claimed = "<del>" + value.daily_allowance_amount_claimed + "</del>" + " " + value.daily_allowance_amount_approved;
                                        total_da += parseInt(value.daily_allowance_amount_approved);
                                    } else {
                                        daily_allowance_amount_claimed = value.daily_allowance_amount_claimed;
                                        total_da += parseInt(value.daily_allowance_amount_claimed);

                                    }
                                }

                                if ((value.telephone_postage_amount_claimed == null) || (value.telephone_postage_amount_claimed == "")) {
                                    telephone_postage_amount_claimed = "-";

                                } else {
                                    if (value.telephone_postage_amount_approved && value.telephone_postage_amount_approved != null && value.telephone_postage_amount_approved != value.telephone_postage_amount_claimed) {
                                        telephone_postage_amount_claimed = "<del>" + value.telephone_postage_amount_claimed + "</del>" + " " + value.telephone_postage_amount_approved;
                                        total_tel += parseInt(value.telephone_postage_amount_approved);
                                    } else {
                                        telephone_postage_amount_claimed = value.telephone_postage_amount_claimed;
                                        total_tel += parseInt(value.telephone_postage_amount_claimed);
                                    }

                                }
                                //console.log(value);
                                if (value.vehicle_type == 0 || value.vehicle_type == 1) {
                                    var getpersonalVechicleData = (parseInt(meter_to1) - parseInt(meter_from1));
                                    travel_amount_claimed1 = value.self_total_fare;
                                    total_pv += parseInt(value.self_total_fare);
                                } else {
                                    travel_amount_claimed1 = '0';
                                }

                                if ((value.misc_amount_claimed == null) || (value.misc_amount_claimed == "")) {
                                    misc_amount_claimed = "-";

                                } else {
                                    if (value.misc_amount_approved && value.misc_amount_approved != null && value.misc_amount_claimed != value.misc_amount_approved) {
                                        misc_amount_claimed = "<del>" + value.misc_amount_claimed + "</del>" + " " + value.misc_amount_approved;
                                        total_misc += parseInt(value.misc_amount_approved);
                                    } else {
                                        misc_amount_claimed = value.misc_amount_claimed;
                                        total_misc += parseInt(value.misc_amount_claimed);

                                    }
                                }

                                if (value.location_from !== null && (value.location_from_id === null || value.location_from_id == '')) {
                                    output = '<tr><td>' + value.submission_date.split('-').reverse().join('-') + '</td><td class="' + i + '" style="position:relative;" name="' + value.location_from_id + '">' + location_from + '</td><td name="' + value.location_from_id + '" class="' + i + '" style="position:relative;">' + location_to + '</td>';
                                } else if (value.location_to !== null && (value.location_to_id === null || value.location_to_id == '')) {
                                    output = '<tr><td>' + value.submission_date.split('-').reverse().join('-') + '</td><td class="' + i + '" style="position:relative;" name="' + value.location_from_id + '">' + location_from + '</td><td name="' + value.location_from_id + '" class="' + i + '" style="position:relative;">' + location_to + '</td>';
                                } else if (value.total_claim_amount !== null || value.total_claim_amount > 0) {
                                    output = '<tr><td>' + value.submission_date.split('-').reverse().join('-') + '</td><td class="' + i + '" style="position:relative;" name="' + value.location_from_id + '">' + location_from + '</td><td name="' + value.location_from_id + '" class="' + i + '" style="position:relative;">' + location_to + '</td>';

                                } else {
                                    output = '<tr><td>' + value.submission_date.split('-').reverse().join('-') + '</td><td class="' + i + '"  name="' + value.location_from_id + '">' + location_from + '</td><td name="' + value.location_from_id + '" class="' + i + '" style="position:relative;">' + location_to + '</td>';
                                }

                                output += '<td><table class="table-bordered table-striped table-vcenter custom-table2" style="font-size:10pt;">';

                                output += '<tr><td>' + value.meter_from + '</td><td style="padding-left: 3px; padding-right: 3px;" border="0">-</td><td>' + value.meter_to + '</td></tr></table>';
                                output += '</td><td><table class="table-bordered table-striped table-vcenter custom-table2" style="font-size:10pt;">';
                                output += '<td>' + train_amount_claimed + '</td><td>' + bus_amount_claimed + '</td><td>' + air_amount_claimed + '</td><td>' + auto_taxi_claimed + '</td></tr></table></td>';
                                //output += '<td><table class="table-bordered table-striped table-vcenter custom-table2"><tr><td class="inner-table">CV</td></tr>';
                                output += '<td>' + fuel_amount_claimed + '</td>';
                                output += '<td>' + travel_amount_claimed1 + '</td>';

                                output += '<td>' + hotel_amount_claimed + '</td><td>' + toll_amount_claimed + '</td>';
                                output += '<td>' + daily_allowance_amount_claimed + '</td><td>' + telephone_postage_amount_claimed + '</td><td>' + misc_amount_claimed + '</td><td>' + total_claim_amount + '</td></tr>';

                                $("#day_detail").append(output);
                            } else {
                                if (value.is_force_claim == 1) {
                                    fncday = value.submission_day;
                                    fnc = fnc.concat(value.submission_day);
                                    fnc = fnc.concat(", ");
                                } else {
                                    fncday = value.submission_day;
                                    zeroc = zeroc.concat(value.submission_day);
                                    zeroc = zeroc.concat(", ");
                                }
                            }

                            i++;
                        });
                    } else {
//                                                data_append = '<tr><td colspan="6">No data to display</td></tr>';
//                                                $("#user_info").html(data_append);
                        console.log(responsedata);

                    }

                    var total_values_row = '<tr style="border: 1px solid black;"><td></td><td class="1" style="position:relative;" name="null"><strong>TOTAL : </strong></td><td name="null" class="1" style="position:relative;"></td><td><strong>' + total_distance + '</strong></td><td><strong>' + total_tr + ' | ' + total_bus + ' | ' + total_fl + ' | ' + total_au + '</strong></td><td><strong>' + total_cv + '</strong></td><td><strong>' + total_pv + '</strong></td><td><strong>' + total_hotel + '</strong></td><td><strong>' + total_toll + '</strong></td><td><strong>' + total_da + '</strong></td><td><strong>' + total_tel + '</strong></td><td><strong>' + total_misc + '</strong></td><td><strong>' + net_total + '</strong></td></tr>';
                    $("#day_detail").append(total_values_row);
                    if (fnc.length > 1) {
                        fncstr = '<tr><td colspan="10">' + "Employee can not submit claim for: " + fnc + '</td></tr>';
                        $("#day_detail").append(fncstr);
                    }
                    var add_form_data = '<div class="col-xs-4" style=""><b>Kms. Done: ' + total_distance + '</b></div><div class="col-xs-4" style=""><b>Approved Amount:</b>...........</div>';
                    $("#calculated_values").append(add_form_data);

                    // var add_form_data2 ='<div class="col-xs-4" style=""><b>Employee&rsquo;s Signature:</b>........................</div><div class="col-xs-4" style=""><b>Fuel Used : Rs '+ total_cv +'</b></div><div class="col-xs-4" style=""><b>Approver&rsquo;s Signature & Date:</b>..........................</div>';
                    if (sub_status == 3) {
                        if (approval_details['approver_id'] != "") {
                            var add_form_data2 = '<div class="col-xs-4" style=""><b>Employee&rsquo;s Signature:</b>........................</div><div class="col-xs-4" style=""><b>Fuel Used : Rs ' + total_cv + '</b></div><div class="col-xs-4" style=""><b>Approver&rsquo;s Name & Date: </b>' + approval_details['approver_name'] + ' - ' + approval_details['approved_on'] + '</div>';
                        } else {
                            var add_form_data2 = '<div class="col-xs-4" style=""><b>Employee&rsquo;s Signature:</b>........................</div><div class="col-xs-4" style=""><b>Fuel Used : Rs ' + total_cv + '</b></div><div class="col-xs-4" style=""><b>Approver&rsquo;s Name & Date:</b>..........................</div>';
                        }
                    } else {
                        var add_form_data2 = '<div class="col-xs-4" style=""><b>Employee&rsquo;s Signature:</b>........................</div><div class="col-xs-4" style=""><b>Fuel Used : Rs ' + total_cv + '</b></div>';
                    }


                    $("#calculated_values").append(add_form_data2);

                    if (zeroc.length > 1) {
                        fncstr = '<tr><td colspan="14">' + "No claim submitted by Employee for: " + zeroc + '</td></tr>';
                        $("#day_detail").append(fncstr);

                    }
                } else {
//                                            $("#user_name").html('No employee with this code exits.').css({"font-size":"15px"});
//                                            data_append = '<tr><td colspan="6">' + responsedata.data.error_msg + '</td></tr>';
//                                            $("#user_info").html(data_append);
                    console.log(responsedata);
                }

                setTimeout(function () {
                    $('.overlay').hide();
                }, 1000);
            },
            error: function (e) {
                console.log(e);
                setTimeout(function () {
                    $('.overlay').hide();
                }, 1000);
            },
            beforeSend: function () {
                $(".overlay").show();
            }

        });

    });
</script>
</body>
</html>